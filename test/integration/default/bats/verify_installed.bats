@test "emacs is installed and in the path" {
  command -v emacs
}

@test "emacs major version is 24" {
  run bash -c "emacs --version | head -n1 | awk '{print \$NF}' | cut -d'.' -f1"
  [ "$output" = "24" ]
}
