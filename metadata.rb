name 'fg-emacs24'
maintainer 'Steve Buzonas'
maintainer_email 'steve@fancyguy.com'
license 'mit'
description 'Installs emacs24'
long_description 'Installs emacs24'
version '0.1.0'

depends 'apt'

supports 'ubuntu'
